#! /bin/bash

curl -X POST \
  https://fcm.googleapis.com/fcm/send \
  -H 'Authorization: key=xxxxx' \
  -H 'Content-Type: application/json' \
  -d '{
    "to" : "eudfDn_yVLxVT0wCPp_aH1:APA91bFIJfu8b7-m8oSCBCrMFtT-mj7M3Uut_HwnI3zo1QQNo9D1l6sb8ufWD_VzDx-Au1tLi4-rSvzJEvBndcVPkeX-anB730rE4vWaTCagpuREBbJRTOzbR2il3DbOtgLnUoy0typH",
        "data": {
        "message": "Hello World!"
    },
    "notification": {
        "title": "Hello World",
        "body": "This is FCM Message from Admin"
        "click_action": "https://peryl-pwa.web.app/#notif",
        "url": "https://peryl-pwa.web.app/#notif"
    }
}'

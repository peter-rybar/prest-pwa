import { HApp, HDispatcher } from "peryl/dist/hsml-app";
import { appShellDispatcher, appShellState, AppShellState, appShellView } from "./components/appshell";
import { formDispatcher, formState, formView } from "./components/form";
import { homeState, homeView } from "./components/home";
import { notifDispatcher, notifState, notifView } from "./components/notif";
import { swInit } from "./lib/service-worker-lib";

HApp.debug = window.location.hostname === "localhost";
// HApp.debug = true;


appShellState.contents = [
    { content: "home", label: "Home", url: "#", icon: "i.fa.fa-fw.fa-home", view: homeView, state: homeState },
    { content: "home-name", label: "Hello Peter", url: "#home/Peter", icon: "i.fa.fa-fw.fa-home", view: homeView, state: homeState },
    { content: "notif", label: "Notification", url: "#notif", icon: "i.fa.fa-fw.fa-bell", view: notifView, state: notifState },
    { content: "form", label: "Form", url: "#form", icon: "i.fa.fa-fw.fa-users", view: formView, state: formState }
];

const appDispatcher: HDispatcher<AppShellState> = (action, state, dispatch) => {
    appShellDispatcher(action, state, dispatch);
    notifDispatcher(action, state.contents.find(c => c.content === "notif").state, dispatch);
    formDispatcher(action, state.contents.find(c => c.content === "form").state, dispatch);
};

const app = new HApp(appShellState, appShellView, appDispatcher).mount();
(self as any).app = app;

swInit();

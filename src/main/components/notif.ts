import { HElements } from "peryl/dist/hsml";
import { HAppAction, HDispatcher, HView } from "peryl/dist/hsml-app";
import { FCM } from "../lib/fcm";
import { PushyMe } from "../lib/pushy";
import { showNotification } from "../lib/service-worker-lib";
import { AppShellAction, AppShellContentState } from "./appshell";

export interface NotifState extends AppShellContentState {
    title: string;
    pushy: PushyMe;
    pushyDeviceToken: string;
    pushyApiKey: string;
    fcm: FCM;
    fcmCurrentToken: string;
    fcmServerKey: string;
}

export enum NotifAction {
    snackbar = "notif-snackbar",
    browser = "notif-browser",
    pushy = "notif-pushy",
    fcm = "notif-fcm"
}

export const notifState: NotifState = {
    title: "Notifications",
    pushyDeviceToken: "",
    pushy: undefined,
    pushyApiKey: "",
    fcm: undefined,
    fcmCurrentToken: "",
    fcmServerKey: ""
};

export const notifView: HView<NotifState> = (state): HElements => [
    ["div.w3-content", [
        ["h1", state.title],
        ["h2", "Snackbar notification"],
        ["form.w3-container", { on: ["submit", NotifAction.snackbar] },
            [
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Snackbar message"
                        }]
                    ]]
                ]],
                ["button.w3-btn.w3-blue", "Send"]
            ]
        ],
        ["h2", "Browser notification"],
        ["form.w3-container", { on: ["submit", NotifAction.browser] },
            [
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Browser message"
                        }]
                    ]]
                ]],
                ["button.w3-btn.w3-blue", "Send"]
            ]
        ],
        ["h2", "Pushy notification"],
        ["form.w3-container", { on: ["submit", NotifAction.pushy] },
            [
                ["p", [
                    ["label", [
                        "Device token",
                        ["input.w3-input", {
                            type: "text",
                            name: "device",
                            value: state.pushyDeviceToken,
                            disabled: "disabled"
                        }]
                    ]],
                ]],
                ["p", [
                    ["label", [
                        "API key",
                        ["input.w3-input", {
                            type: "text",
                            name: "apiKey",
                            value: state.pushyApiKey
                        }]
                    ]],
                ]],
                ["p", [
                    ["label", [
                        "Title",
                        ["input.w3-input", {
                            type: "text",
                            name: "title",
                            value: "Title"
                        }]
                    ]]
                ]],
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Message"
                        }]
                    ]]
                ]],
                ["p", [
                    ["label", [
                        "URL",
                        ["input.w3-input", {
                            type: "text",
                            name: "url",
                            value: ""
                        }]
                    ]]
                ]],
                ["p", [
                    ["button.w3-btn.w3-blue", "Send"]
                ]]
            ]
        ],
        ["h2", "FCM notification"],
        ["form.w3-container", { on: ["submit", NotifAction.fcm] },
            [
                ["p", [
                    ["label", [
                        "Current token",
                        ["input.w3-input", {
                            type: "text",
                            name: "device",
                            value: state.fcmCurrentToken,
                            disabled: "disabled"
                        }]
                    ]],
                ]],
                ["p", [
                    ["label", [
                        "Server key",
                        ["input.w3-input", {
                            type: "text",
                            name: "serverKey",
                            value: state.fcmServerKey
                        }]
                    ]],
                ]],
                ["p", [
                    ["label", [
                        "Title",
                        ["input.w3-input", {
                            type: "text",
                            name: "title",
                            value: "Title"
                        }]
                    ]]
                ]],
                ["p", [
                    ["label", [
                        "Message",
                        ["input.w3-input", {
                            type: "text",
                            name: "message",
                            value: "Message"
                        }]
                    ]]
                ]],
                ["p", [
                    ["label", [
                        "URL",
                        ["input.w3-input", {
                            type: "text",
                            name: "url",
                            value: ""
                        }]
                    ]]
                ]],
                ["p", [
                    ["button.w3-btn.w3-blue", "Send"]
                ]]
            ]
        ]
    ]]
];

const pushyApiKey = "pushyApiKey";
const fcmServerKey = "fcmServerKey";

export const notifDispatcher: HDispatcher<NotifState> = (action, state, dispatch) => {
    switch (action.type) {

        case HAppAction._init:
            // const pushy = new Pushy(
            //     "5dca8013574cb62d339c084f",
            //     (deviceToken) => {
            //         state.pushyDeviceToken = deviceToken;
            //     },
            //     (data) => {
            //         console.log("Pushy received notification: " + JSON.stringify(data));
            //         const message = `${data.title}: ${data.message}`;
            //         dispatch(AppShellAction.message, message);
            //     });
            // state.pushy = pushy;
            // (window as any).pushy = pushy;

            // https://console.firebase.google.com/u/0/project/peryl-pwa/settings/general/web:MTUyODE1ZmEtNmE1Ny00ZWQzLTg5MWItNWQwNzExOTFhNjcw
            const firebaseConfig = {
                apiKey: "AIzaSyD-hBHDXO6-0Z4qs6LfdjvcEP96wjxDDJE",
                authDomain: "peryl-happ.firebaseapp.com",
                projectId: "peryl-happ",
                storageBucket: "peryl-happ.appspot.com",
                messagingSenderId: "148251492373",
                appId: "1:148251492373:web:6af29e6c694aa8cce1ce1c"
            };
            const vapidKey = "BAwITFlgwDvkBoQek9IHOjcTLSpGBT8mpBmCT6mgsFsFf8RG03vU94NonremJp9bb7RMJtGTBYaEfyjOpGDeEC0";
            const fcm = new FCM(firebaseConfig, vapidKey,
                (currentToken) => {
                    state.fcmCurrentToken = currentToken;
                },
                (data) => {
                    console.log("FCM received notification: " + JSON.stringify(data));
                    const message = `${data.notification.title}: ${data.notification.body}`;
                    dispatch(AppShellAction.message, message);
                });
            state.fcm = fcm;
            (window as any).fcm = fcm;

            state.pushyApiKey = localStorage.getItem(pushyApiKey) || "";
            state.fcmServerKey = localStorage.getItem(fcmServerKey) || "";
            break;

        case NotifAction.browser:
            browserNotif(action.data.message);
            break;

        case NotifAction.snackbar:
            dispatch(AppShellAction.message, action.data.message);
            break;

        case NotifAction.pushy:
            action.data.apiKey &&
                localStorage.setItem(pushyApiKey, action.data.apiKey);
            state.pushy.push(
                action.data.apiKey,
                action.data.title,
                action.data.message,
                action.data.url);
            break;

        case NotifAction.fcm:
            action.data.serverKey &&
                localStorage.setItem(fcmServerKey, action.data.serverKey);
            state.fcm.push(
                action.data.serverKey,
                action.data.title,
                action.data.message,
                action.data.url);
            break;
        }
};

function browserNotif(message: string) {
    showNotification(message, {
        body: "Notification body",
        // icon: "images/icon.png",
        // badge: "images/badge.png",
        vibrate: [100, 50, 100],
        data: {
            onAction: {
                // "": location.href,
                like: "http://bbb.sk",
                reply: "http://ccc.sk"
            }
        },
        actions: [
            { action: "like", title: "Like" },
            { action: "reply", title: "Reply" }
        ]
    });
}

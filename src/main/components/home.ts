import { HElements } from "peryl/dist/hsml";
import { HView } from "peryl/dist/hsml-app";
import { AppShellContentState } from "./appshell";

export interface HomeState extends AppShellContentState {
    title: string;
    name: string;
}

export const homeState: HomeState = {
    title: "Home",
    name: ""
};

export const homeView: HView<HomeState> = (state): HElements => [
    ["div.w3-content", [
        ["h1", state.title],
        ["p", `Hello ${state.name}`]
    ]]
];

// import { Hash } from "peryl/dist/hash";
import { HElement, HElements } from "peryl/dist/hsml";
import { HAppAction, HDispatcher, HView } from "peryl/dist/hsml-app";
import { Router } from "peryl/dist/router";

const nbsp = "\u00a0 ";

export interface AppShellState {
    title: string;
    subtitle: string;
    menu: boolean;
    content: string;
    contents: {
        content: string;
        label: string;
        url: string;
        icon: string;
        view: HView<any>;
        state: any;
    }[];
    snackbar: string;
}

export const appShellState: AppShellState = {
    title: "PeRyL PWA",
    subtitle: "",
    menu: false,
    content: "home",
    contents: [],
    snackbar: undefined
};

export const enum AppShellAction {
    menu = "appshell-menu",
    content = "appshell-content",
    message = "appshell-message",
}

export const appShellView: HView<AppShellState> = (state): HElements => {
    const content = state.contents.find(content => content.content === state.content);
    return [
        // header
        ["div.w3-bar.w3-top.w3-large.w3-blue.w3-card", { style: "z-index:4" }, [
            ["button.w3-bar-item.w3-button.w3-hide-large.w3-hover-none.w3-hover-text-light-grey",
                {
                    accesskey: "m",
                    "aria-label": "Menu",
                    on: ["click", AppShellAction.menu, null]
                },
                [["i.fa.fa-bars"]],
            ],
            ["span.w3-bar-item", [
                ["strong.w3-hide-small", [
                    ["a", { href: "#", style: "text-decoration: none;" },
                        state.title
                    ]
                ]],
                ["span.w3-hide-small", state.subtitle ? " - " : ""],
                ["span", state.subtitle ? state.subtitle : ""],
            ]],
            ["a.w3-bar-item.w3-right.w3-hover-light-grey",
                {
                    href: "https://gitlab.com/peter-rybar/pwa",
                    title: "GitLab repository",
                    target: "_blank",
                    rel: "noopener"
                },
                [["i.fa.fa-gitlab"]]
            ]
        ]],
        // sidebar
        ["div#sidebar.w3-sidebar.w3-collapse.w3-card.w3-animate-left~sidebar",
            {
                styles: {
                    zIndex: "3",
                    width: "300px",
                    display: state.menu ? "block" : "none"
                }
            },
            [
                ["div.w3-container", [
                    ["h2", [state.title]],
                    ["div.w3-bar-block", {},
                        state.contents.map<HElement>(c => (
                            ["a.w3-bar-item.w3-button.w3-padding",
                                {
                                    href: c.url,
                                    classes: [["w3-light-grey", c.content === `#${state.content}`]]
                                },
                                [
                                    [c.icon], nbsp, c.label
                                ]
                            ])
                        )
                    ]
                ]]
            ]
        ],
        // overlay
        ["div#overlay.w3-overlay.w3-hide-large.w3-animate-opacity~overlay",
            {
                styles: {
                    cursor: "pointer",
                    display: state.menu ? "block" : "none"
                },
                title: "close side menu",
                on: ["click", AppShellAction.menu, null]
            }
        ],
        // main
        ["div.w3-main", { style: "margin-left:300px;margin-top:43px;" }, [
            ["div.w3-container~content",
                content.view(content.state)
            ]
        ]],
        // snackbar
        ["div#snackbar~snackbar", { classes: [["show", !!state.snackbar]] },
            state.snackbar
        ]
    ];
};

export const appShellDispatcher: HDispatcher<AppShellState> = (action, state, dispatch) => {
    switch (action.type) {

        case HAppAction._mount:
            Router
                .route(":content", (content: string) => {
                    console.log("route: ':content'", content);
                    dispatch(AppShellAction.content, content);
                })
                .route(":content/:name", (content: string, name: string) => {
                    console.log("route ':content/:text'", content, name);
                    name && (state.contents.find(c => c.content === content).state.name = name);
                    dispatch(AppShellAction.content, content);
                })
                .route("*", (path: string) => {
                    console.log("route '*'", path);
                    dispatch(AppShellAction.content);
                });
            Router.start();
            // Router.navigate("home/Peter");

            // new Hash<string>()
            //     // .coders(
            //     //     data => data ? JSON.stringify(data) : "",
            //     //     str => str ? JSON.parse(str) : undefined
            //     // )
            //     .onChange(module => {
            //         console.log("hash: " + JSON.stringify(module));
            //         state.sidebar.menuActive = module;
            //         state.content = module;
            //         state.menu = false;
            //         update();
            //     })
            //     .listen();

            setTimeout(() => dispatch(AppShellAction.message, "Init message"), 1e3);
            break;

        case AppShellAction.menu:
            state.menu = action.data === null ? !state.menu : action.data;
            break;

        case AppShellAction.content:
            const content = state.contents.find(c => c.content === action.data)
                ? action.data
                : state.contents[0].content;
            state.content = content;
            state.menu = false;
            state.subtitle = state.contents.find(c => c.content === content).state.title;
            break;

        case AppShellAction.message:
            state.snackbar = action.data;
            if (action.data) {
                setTimeout(() => dispatch(AppShellAction.message, undefined), 3e3);
            }
            break;
        }
};

export interface AppShellContentState {
    title: string;
}

export const appShellContentState: AppShellContentState = {
    title: "Title"
};

export const appShellContentView: HView<AppShellContentState> = (state): HElements => [
    ["div.w3-content", [
        ["h1", state.title],
    ]]
];

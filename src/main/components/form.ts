import { HElement, HElements, hjoin } from "peryl/dist/hsml";
import { HAppAction, HDispatcher, HView } from "peryl/dist/hsml-app";
import { BooleanValidator, DateValidator, FormValidator, FormValidatorData, NumberValidator, SelectValidator, StringValidator } from "peryl/dist/validators";
import { AppShellContentState } from "./appshell";

export interface FormData {
    name: string;
    born: Date;
    children: number;
    married: boolean;
    gender: string;
    sport: string;
}

export interface FormState extends AppShellContentState {
    title: string;
    data: FormData;
    genders: {
        label: string;
        value: string;
    }[];
    sports: string[];
    validatorData?: FormValidatorData<FormData>;
}

export const formState: FormState = {
    title: "Form",
    data: {
        name: "Ema",
        born: undefined,
        children: 0,
        married: false,
        gender: "female",
        sport: "gymnastics"
    },
    genders: [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" }
    ],
    sports: ["football", "gymnastics"]
};

export enum FormAction {
    change = "form-change",
    submit = "form-submit"
}

export const formView: HView<FormState> = (state): HElements => [
    ["div.w3-content", [
        ["h1", state.title],
        ["form.w3-container",
            {
                on: [
                    ["change", FormAction.change],
                    ["submit", FormAction.submit]
                ]
            },
            [
                ["p", [
                    ["label", ["Name",
                        ["input.w3-input", {
                            type: "text",
                            name: "name",
                            value: state.validatorData.obj.name
                        }]
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.name]],
                ["p", [
                    ["label", ["Born",
                        ["input.w3-input", {
                            type: "datetime-local",
                            name: "born",
                            value: state.validatorData.obj.born
                        }]
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.born]],
                ["p", [
                    ["label", ["Children",
                        ["input.w3-input", {
                            type: "number",
                            name: "children",
                            value: state.validatorData.obj.children
                        }]
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.children]],
                ["p", [
                    ["label", [
                        ["input.w3-check", {
                            type: "checkbox",
                            name: "married",
                            checked: state.validatorData.obj.married
                        }],
                        " Married"
                    ]]
                ]],
                ["p.w3-text-red", [state.validatorData.err.married]],
                ["p",
                    hjoin(
                        state.genders.map<HElement>(gender => (
                            ["label", [
                                ["input.w3-radio", {
                                    type: "radio",
                                    name: "gender",
                                    value: gender.value,
                                    checked: state.validatorData.obj.gender === gender.value
                                }],
                                " ", gender.label
                            ]]
                        )),
                        ["br"]
                    )
                ],
                ["p.w3-text-red", [state.validatorData.err.gender]],
                ["p", [
                    ["select.w3-select", { name: "sport" },
                        [
                            ["option",
                                { value: "", disabled: true, selected: true },
                                "Sport"
                            ],
                            ...state.sports.map<HElement>(sport => (
                                ["option",
                                    {
                                        value: sport,
                                        selected: sport === state.validatorData.obj.sport
                                    },
                                    sport
                                ])
                            )
                        ]
                    ]
                ]],
                ["p.w3-text-red", state.validatorData.err.sport],
                ["button.w3-btn.w3-blue", "Submit"]
            ]
        ]
    ]]
];

let formValidator: FormValidator<FormData>;

export const formDispatcher: HDispatcher<FormState> = (action, state) => {
    switch (action.type) {

        case HAppAction._mount:
            formValidator = validator(state);
            state.validatorData = formValidator.data();
            break;

        case FormAction.change:
            formValidator.validate({
                ...state.validatorData.str,
                ...action.data
            });
            state.validatorData = formValidator.data();
            console.log("obj:", JSON.stringify(state.validatorData, null, 4));
            break;

        case FormAction.submit:
            action.event.preventDefault();
            formValidator.validate({});
            state.validatorData = formValidator.data();
            if (formValidator.valid) {
                console.log(formValidator);
                state.data = formValidator.obj;
                const formData = JSON.stringify(state.data, null, 4);
                console.dir(formData);
                alert(`Form submit: \n${formData}`);
            }
            break;
    }
};

export function validator(formState: FormState): FormValidator<FormData> {
    return new FormValidator<FormData>()
        .addValidator("name", new StringValidator(
            {
                required: true,
                min: 3,
                max: 5
            },
            {
                required: "Required {{min}} {{max}} {{regexp}}",
                invalid_format: "Invalid format {{regexp}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("born", new DateValidator(
            {
                required: true,
                // min: new Date(),
                max: new Date(),
                dateOnly: false
            },
            {
                required: "required {{min}} {{max}}",
                invalid_format: "invalid_format {{date}}",
                not_in_range: "not_in_range {{min}} {{max}}"
            }))
        .addValidator("children", new NumberValidator(
            {
                required: true,
                min: 0,
                max: 10,
            },
            {
                required: "Required {{min}} {{max}} {{locale}} {{format}}",
                invalid_format: "Invalid format {{num}} {{locale}} {{format}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("gender", new SelectValidator(
            {
                required: true,
                options: formState.genders.map(g => g.value)
            },
            {
                required: "Required {{options}}",
                invalid_option: "Invalod option {{option}} {{options}}"
            }))
        .addValidator("married", new BooleanValidator(
            {
                required: true
            },
            {
                required: "Required",
                invalid_value: "Invalid value {{value}}"
            }))
        .addValidator("sport", new SelectValidator(
            {
                required: true,
                options: formState.sports
            },
            {
                required: "Required {{options}}",
                invalid_option: "Invalod option {{option}} {{options}}"
            }))
        .format(formState.data);
}

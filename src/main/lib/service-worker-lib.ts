// Register Service Worker.
export function swInit(): void {
    if ("serviceWorker" in navigator) {
        // Path is relative to the origin, not project root.
        navigator.serviceWorker.register("./service-worker.js")
            .then(registration => {
                // updateViaCache: "none";
                // registration.update();
                // console.log("Service worker", registration);
                // if (registration.installing) {
                //     console.log("Service worker installing");
                // } else if (registration.waiting) {
                //     console.log("Service worker installed");
                // } else if (registration.active) {
                //     console.log("Service worker active");
                // }
                // console.log("Service worker scope", registration.scope);
            })
            .catch(error => {
                console.error("Service worker register error", error);
            });
    }
}

export function showNotification(title: string, options?: NotificationOptions): void {
    if ((self as any).Notification) {
        // console.log("Notification supported");
        Notification.requestPermission(permission => {
            if (permission === "granted") {
                // console.log("Notification permission granted");
                navigator.serviceWorker.ready.then(registration => {
                    // console.log("Notification SW ready");
                    registration.showNotification(title, options);
                    // registration.addEventListener("updatefound", e => {
                    //     console.log("Registration updatefound", e);
                    //     registration.update();
                    // });
                });
            } else {
                console.warn("Notification permission:", permission);
            }
        });
    } else {
        console.warn("Notification not supported");
    }
}

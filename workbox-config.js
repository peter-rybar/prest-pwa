module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{css,woff2,png,js,html,json}"
  ],
  "swSrc": "dist/sw.js",
  "swDest": "dist/service-worker.js",
  "globIgnores": [
    "../workbox-config.js"
  ]
};

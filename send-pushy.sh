#! /bin/bash

curl -X POST \
  https://api.pushy.me/push?api_key=xxxxx \
  -H 'Content-Type: application/json' \
  -d '{
    "to" : "e6d9ff93f8e114c4aa4dbe",
    "data": {
        "title": "Test Notification",
        "message": "Hello World!",
        "url": "https://peryl-pwa.web.app/#notif"
    },
    "notification": {
        "body": "Hello World \u270c",
        "badge": 1,
        "sound": "ping.aiff"
    }
}'
